EESchema Schematic File Version 4
LIBS:usbpower-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 2500 2750 1250 750 
U 5E41ABC1
F0 "MCU" 50
F1 "MCU.sch" 50
$EndSheet
$Sheet
S 5700 2800 1100 700 
U 5E45A090
F0 "USB_Power" 50
F1 "USB_Power.sch" 50
$EndSheet
$EndSCHEMATC
