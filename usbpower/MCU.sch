EESchema Schematic File Version 4
LIBS:usbpower-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_ST_STM32F1:STM32F103C6Tx U?
U 1 1 5E4422E7
P 2650 3150
AR Path="/5E4422E7" Ref="U?"  Part="1" 
AR Path="/5E41ABC1/5E4422E7" Ref="U1"  Part="1" 
F 0 "U1" H 2600 1561 50  0000 C CNN
F 1 "STM32F103C6Tx" H 2600 1470 50  0000 C CNN
F 2 "Package_QFP:LQFP-48_7x7mm_P0.5mm" H 2050 1750 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00210843.pdf" H 2650 3150 50  0001 C CNN
	1    2650 3150
	1    0    0    -1  
$EndComp
$EndSCHEMATC
